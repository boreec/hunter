#include "Hunter.hpp"
#include <iostream>

Hunter::Hunter(uint16_t x, uint16_t y, Environment* e) : Agent(x,y,e){

}

void Hunter::decide(){
    std::vector<std::vector<std::vector<int>>> grids = _environment->getTargetsDjikstra();
    if(!grids.size()){
        //no targets found, don't decide anything.
        return;
    }
    int min = _environment->getHeight() + _environment->getWidth();
    std::pair<uint16_t, uint16_t> target_square(_x, _y);

    for(uint16_t g = 0; g < grids.size(); g++){
        std::vector<std::pair<uint16_t, uint16_t>> neighbours = _environment->getNeighbours(_x, _y);
        for(uint16_t n = 0; n < neighbours.size(); n++){
            //if there is no agent in the environment at this place.
            if(grids[g][neighbours[n].second][neighbours[n].first] <= min){
                Agent* a = _environment->getAgent(neighbours[n].first, neighbours[n].second);
                if(a && a->huntable()){
                    a->setAlive(false);
                    return;
                }
                if(a == nullptr){
                    min = grids[g][neighbours[n].second][neighbours[n].first];
                    target_square = neighbours[n];
                }
            }
        }
    }

    _environment->moveAgent(_x, _y, target_square.first, target_square.second);

}

std::string Hunter::to_string() const{
    return "H";
}

QColor Hunter::getColor() const{
    return QColor(0, 255, 0);
}

bool Hunter::huntable() const{
    return false;
}
