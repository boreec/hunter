#ifndef WALL_HPP
#define WALL_HPP
#include "Agent.hpp"

class Wall : public Agent
{
public:
    Wall(uint16_t x, uint16_t y, Environment *e);
    virtual void decide() override;
    virtual std::string to_string() const override;
    virtual QColor getColor() const override;
    virtual bool huntable() const override;
};

#endif // WALL_HPP
