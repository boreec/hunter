#include "Wall.hpp"

Wall::Wall(uint16_t x, uint16_t y, Environment *e) : Agent(x,y,e){

}
void Wall::decide(){
    //do nothing
}

std::string Wall::to_string() const {
    return "W";
}

QColor Wall::getColor() const{
    return QColor(153, 77, 0);
}

bool Wall::huntable() const{
    return false;
}
