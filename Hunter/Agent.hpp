#ifndef AGENT_HPP
#define AGENT_HPP
#include <string>
#include <QColor>
#include "Environment.hpp"

class Environment;

class Agent
{
private:
    std::vector<std::vector<int>> djikstra_grid;
protected:
    uint16_t _x;
    uint16_t _y;
    Environment* _environment;
    bool alive;
    /*
     * Compute the Djikstra distances for the current Agent.
     * If reverse is set to true, the shortest path is inversed:
     * Agent position has the highest value and the farthest positions
     * have the lowest value. */
    void computeDjikstra(bool reverse = false);
public:
    Agent(uint16_t x = 0, uint16_t y = 0, Environment* e = nullptr);
    virtual void decide() = 0;
    virtual std::string to_string() const = 0;
    virtual QColor getColor() const = 0;
    virtual uint64_t getEnergy() const;
    virtual bool huntable() const = 0;
    virtual bool eatable() const;
    virtual bool winnable() const;
    std::vector<std::vector<int>> getDjikstraGrid() const;
    void setX(uint16_t);
    void setY(uint16_t);
    void setAlive(bool b);
    bool isAlive() const;
};

#endif // AGENT_HPP
