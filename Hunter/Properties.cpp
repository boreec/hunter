#include "Properties.hpp"

Properties::Properties(){
  // Agent properties
  HUNTERS_QUANTITY = 3;
  AVATAR_QUANTITY = 3;
  DEFENDER_LIFE = 6;

  // Environment properties
  TORUS = true;
  ENVIRONMENT_WIDTH = 20;
  ENVIRONMENT_HEIGHT = 20;
  WALLS_PERCENT = 20;
  SEED = 0;

  // View properties
  VIEW_WIDTH = 1000;
  VIEW_HEIGHT = 900;
  CELL_SIZE = 40;
  GRID = true;

  // Scheduling properties
  SCHEDULER = "SEQUENTIAL";
  DELAY = 500;
  TICK_LIMIT = false;
  CSV_EXPORT = false;
}

std::string Properties::to_string(){
  std::string res = "";
  res += "== Agent properties ==\n";
  res += "== Environment properties ==\n";
  res += "HUNTER_QUANTITY: " + std::to_string(HUNTERS_QUANTITY) + "\n";
  res += "AVATAR_QUANTITY: " + std::to_string(AVATAR_QUANTITY) + "\n";
  res += "DEFENDER_LIFE: " + std::to_string(DEFENDER_LIFE) + "\n";
  res += "TORUS:" + std::to_string(TORUS) + "\n";
  res += "ENVIRONMENT_WIDTH:" + std::to_string(ENVIRONMENT_WIDTH) + "\n";
  res += "ENVIRONMENT_HEIGHT:" + std::to_string(ENVIRONMENT_HEIGHT) + "\n";
  res += "WALLS_PERCENT:" + std::to_string(WALLS_PERCENT) + "\n";
  res += "SEED:" + std::to_string(SEED) + "\n";
  res += "== View properties ==\n";
  res += "VIEW_WIDTH:" + std::to_string(VIEW_WIDTH) + "\n";
  res += "VIEW_HEIGHT:" + std::to_string(VIEW_HEIGHT) + "\n";
  res += "CELL_SIZE:" + std::to_string(CELL_SIZE) + "\n";
  res += "GRID:" + std::to_string(GRID) + "\n";
  res += "== Scheduling properties ==\n";
  res += "SCHEDULER:" + SCHEDULER + "\n";
  res += "DELAY:" + std::to_string(DELAY) + "\n";
  res += "TICK_LIMIT:" + std::to_string(TICK_LIMIT) + "\n";
  res += "== Features properties ==\n";
  res += "CSV_EXPORT:" + std::to_string(CSV_EXPORT) + "\n";
  return res;
}
