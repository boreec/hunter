#ifndef AVATAR_HPP
#define AVATAR_HPP
#include "Agent.hpp"

class Avatar : public Agent
{
private:
    bool won;
    bool moveRandomly();
    bool moveOnGlobalDirection();
    uint16_t defender_eaten;
    uint16_t immunity_duration;
public:
    Avatar(uint16_t x, uint16_t y, Environment *e);

    virtual void decide() override;
    virtual std::string to_string() const override;
    virtual QColor getColor() const override;
    virtual bool huntable() const override;
    virtual uint64_t getEnergy() const override;
    virtual bool winnable() const override;
};

#endif // AVATAR_HPP
