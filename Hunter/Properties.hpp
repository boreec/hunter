#include <string>

#ifndef __PROPERTIES_HPP__
#define __PROPERTIES_HPP__
class Properties{
private:
  // Agent properties
  uint16_t HUNTERS_QUANTITY;
  uint16_t AVATAR_QUANTITY;
  uint16_t DEFENDER_LIFE;

  // Environment properties
  bool TORUS;
  uint16_t ENVIRONMENT_WIDTH;
  uint16_t ENVIRONMENT_HEIGHT;
  uint16_t WALLS_PERCENT;
  unsigned int SEED;

  // View properties
  uint16_t VIEW_WIDTH;
  uint16_t VIEW_HEIGHT;
  uint16_t CELL_SIZE;
  bool GRID;

  // Scheduling properties
  std::string SCHEDULER;
  uint16_t DELAY;
  unsigned int TICK_LIMIT;

  // Features properties
  bool CSV_EXPORT;

public:
  Properties();
  std::string to_string();
  void setHunterQuantity(uint16_t h){HUNTERS_QUANTITY = h;};
  void setAvatarQuantity(uint16_t a){AVATAR_QUANTITY = a;};
  void setDefenderLife(uint16_t l){DEFENDER_LIFE = l;};
  void setTorus(bool b){TORUS = b;};
  void setEnvironmentWidth(uint16_t t){ENVIRONMENT_WIDTH = t;};
  void setEnvironmentHeight(uint16_t t){ENVIRONMENT_HEIGHT = t;};
  void setWallsPercent(uint16_t p){WALLS_PERCENT = p;};
  void setSeed(unsigned int s){SEED = s;};
  void setScheduler(std::string s){SCHEDULER = s;};
  void setViewWidth(uint16_t t){VIEW_WIDTH = t;};
  void setViewHeight(uint16_t t){VIEW_HEIGHT = t;};
  void setCellSize(uint16_t s){CELL_SIZE = s;};
  void setGrid(bool b){GRID = b;};
  void setDelay(uint16_t d){DELAY = d;};
  void setTickLimit(unsigned int u){TICK_LIMIT = u;};
  void setCSVExport(bool b){CSV_EXPORT = b;};

  uint16_t getHunterQuantity() const {return HUNTERS_QUANTITY;};
  uint16_t getAvatarQuantity() const {return AVATAR_QUANTITY;};
  uint16_t getDefenderLife() const {return DEFENDER_LIFE;};
  uint16_t getViewWidth() const {return VIEW_WIDTH;};
  uint16_t getViewHeight() const {return VIEW_HEIGHT;};
  std::string getScheduler() const {return SCHEDULER;};
  uint16_t getCellSize() const{return CELL_SIZE;};
  uint16_t getEnvironmentWidth() const {return ENVIRONMENT_WIDTH;};
  uint16_t getEnvironmentHeight() const {return ENVIRONMENT_WIDTH;};
  uint16_t getWallsPercent() const {return WALLS_PERCENT;};
  unsigned int getSeed() const {return SEED;};
  bool getGrid() const {return GRID;};
  uint16_t getDelay() const {return DELAY;};
  unsigned int getTickLimit() const {return TICK_LIMIT;};
  bool getTorus() const {return TORUS;};
};
#endif
