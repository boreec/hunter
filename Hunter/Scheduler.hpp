#ifndef SCHEDULER_HPP
#define SCHEDULER_HPP
#include <vector>
#include "Agent.hpp"

class Scheduler
{
public:
    /*
     * Schedule Agent by the order they appear in the environment. The given
     * targets variable is set to the number of targets found in the environment.
     * */
    static void SCHEDULE_SEQUENTIALLY(const std::vector<Agent*> agents, uint64_t& targets, bool& winnable, bool& won);

    /*
     * Schedule Agent in a random order. The given variable is set to the number of
     * targets found in the environment after all agents acted.
     * */
    static void SCHEDULE_RANDOMLY(const std::vector<Agent*> agents, uint64_t& targets, bool& winnable, bool& won);
};

#endif // SCHEDULER_HPP
