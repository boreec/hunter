#include "Defender.hpp"

Defender::Defender(uint16_t x, uint16_t y, Environment *e, uint16_t life) : Agent(x,y,e){
    _life = life;
}

void Defender::decide(){
    if(_life)
        _life--;
    else
        alive = false;
}

std::string Defender::to_string() const{
    return "D";
}

QColor Defender::getColor() const{
    return Qt::red;
}

bool Defender::huntable() const {
    return false;
};

bool Defender::eatable() const{
    return true;
}
