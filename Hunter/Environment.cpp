#include "Environment.hpp"
#include "Hunter.hpp"
#include "Wall.hpp"
#include <iostream>

Environment::Environment(const Properties* p) : properties(p)
{
    height = properties->getEnvironmentHeight();
    width = properties->getEnvironmentWidth();
    for(uint16_t y = 0; y < height; y++){
        for(uint16_t x = 0; x < width; x++){
            grid.push_back(nullptr);
        }
    }
    if(properties->getSeed()){
        random_engine = new std::default_random_engine(properties->getSeed());
    }else{
        std::random_device a;
        random_engine = new std::default_random_engine(a());
    }
    computeVNNeighbours();
    initialize_walls();
    EDG = EnvironmentGlobalDirection::NONE;
}

void Environment::placeAgents(std::vector<std::unique_ptr<Agent> > &a){
    //shuffle agents to insert them in a random order.
    std::shuffle(a.begin(), a.end(), *random_engine);

    //shuffle coordinates to insert agents somewhere randomly.
    std::vector<std::pair<uint16_t, uint16_t>> coordinates;
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            if(!grid[i * width + j]){
                coordinates.push_back(std::pair<uint16_t, uint16_t>(j,i));
            }
        }
    }
    std::shuffle(coordinates.begin(), coordinates.end(), *random_engine);

    while(a.size() > 0){
        std::pair<uint16_t, uint16_t> random_coordinate = coordinates.back();
        uint16_t agent_x = random_coordinate.first;
        uint16_t agent_y = random_coordinate.second;


        assert(addAgent(std::move(a.back()), agent_x, agent_y));
        coordinates.pop_back();
        a.pop_back();
    }
}

void Environment::computeVNNeighbours(){
    for(int i = 0; i < height; i++){
        std::vector<std::vector<std::pair<uint16_t, uint16_t>>> row;
        for(int j = 0; j < width; j++){
            std::vector<std::pair<uint16_t, uint16_t>> neighbours;
            // Add north neighbour.
            if(i > 0){
                neighbours.push_back(std::pair<uint16_t, uint16_t>(j,i-1));
            }
            // Add south neighbour.
            if(i < height - 1){
                neighbours.push_back(std::pair<uint16_t, uint16_t>(j,i+1));
            }
            // Add east neighbour.
            if(j > 0){
                neighbours.push_back(std::pair<uint16_t, uint16_t>(j-1,i));
            }
            // Add west neighbour.
            if(j < width - 1){
                neighbours.push_back(std::pair<uint16_t, uint16_t>(j+1,i));
            }
            row.push_back(neighbours);
        }
        neighboorhood.push_back(row);
    }
}

void Environment::removeDeadAgents(){
    for(uint16_t i = 0; i < height; i++){
        for(uint16_t j = 0; j < width; j++){
            if(grid[i * width + j] && !grid[i * width + j]->isAlive()){
                grid[i * width + j].reset();
            }
        }
    }
}

void Environment::initialize_walls(){
    // Walls on North and South borders.
    for(uint16_t x = 0; x < width; x++){
        grid[x] = std::make_unique<Wall>(x,0,this);
        grid[(height - 1) * width + x] = std::make_unique<Wall>(x,height-1,this);
    }

    // Walls on East and West borders.
    for(uint16_t y = 1; y < height - 1; y++){
        grid[y * width] = std::make_unique<Wall>(0,y,this);
        grid[y * width + width - 1] = std::make_unique<Wall>(width-1, y,this);
    }

    std::uniform_int_distribution<int> distribution(0, 100);

    // Random walls.
    for(uint16_t y = 1; y < height - 1; y++){
        for(uint16_t x = 1; x < width - 1; x++){
            int r = distribution(*random_engine);
            if(r < properties->getWallsPercent()){
                grid[y * width + x] = std::make_unique<Wall>(x,y,this);
            }
        }
    }
}


void Environment::moveAgent(const uint16_t agent_x, const uint16_t agent_y, const uint16_t target_x, const uint16_t target_y){
    grid[target_y * width + target_x] = std::move(grid[agent_x +  agent_y * width]);
    grid[target_y * width + target_x]->setX(target_x);
    grid[target_y * width + target_x]->setY(target_y);
}

bool Environment::addAgent(std::unique_ptr<Agent> a, uint16_t x, uint16_t y){
    if(!grid[y * width + x]){
        grid[y * width + x] = std::move(a);
        grid[y * width + x]->setX(x);
        grid[y * width + x]->setY(y);
        return true;
    }
    return false;
}

void Environment::removeAgent(uint16_t x, uint16_t y){
    grid[y * width + x].reset();
}

Agent* Environment::getAgent(u_int16_t x, uint16_t y) const{
    return grid[y * width + x].get();
}

const std::vector<Agent*> Environment::getAgents(){
    std::vector<Agent*> res;
    for(uint16_t i = 0; i < height; i++){
        for(uint16_t j = 0; j < width; j++){
            if(grid[i * width + j]){
                res.push_back(grid[i * width + j].get());
            }
        }
    }
    return res;
}

std::vector<std::vector<std::vector<int>>> Environment::getTargetsDjikstra() const{
    std::vector<std::vector<std::vector<int>>> res;
    for(uint16_t i = 0; i < height; i++){
        for(uint16_t j = 0; j < width; j++){
            if(grid[i * width + j] && grid[i * width + j]->huntable()){
                res.push_back(grid[i * width + j]->getDjikstraGrid());
            }
        }
    }
    return res;
}

std::string Environment::to_string() const{
    std::string res = "";
    for(uint16_t y = 0; y < height; y++){
        for(uint16_t x = 0; x < width; x++){
            if(!grid[y * width + x]){
                res += "N\t";
            }else{
                res += grid[y * width + x]->to_string() + "\t";
            }
        }
        res += "\n";
    }
    return res;
}

uint16_t Environment::getWidth() const{
    return width;
}

uint16_t Environment::getHeight() const{
    return height;
}

std::vector<std::pair<uint16_t, uint16_t>> Environment::getNeighbours(uint16_t x, uint16_t y) const{
    return neighboorhood[y][x];
}

std::default_random_engine* Environment::getRandomEngine() const{
    return random_engine;
}

void Environment::setEDG(EnvironmentGlobalDirection edg){
    EDG = edg;
}

EnvironmentGlobalDirection Environment::getEDG() const{
    return EDG;
}

