#ifndef WINNER_HPP
#define WINNER_HPP
#include "Agent.hpp"

class Winner : public Agent{
public:
    Winner(uint16_t x, uint16_t y, Environment *e);

    virtual void decide() override;
    virtual std::string to_string() const override;
    virtual QColor getColor() const override;
    virtual bool huntable() const override;
    virtual bool eatable() const override;
    virtual bool winnable() const override;
};

#endif // WINNER_HPP
