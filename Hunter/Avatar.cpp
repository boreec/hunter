#include "Avatar.hpp"
#include <iostream>
Avatar::Avatar(uint16_t x, uint16_t y, Environment* e) : Agent(x,y,e){
    defender_eaten = 0;
    immunity_duration = 0;
    won = false;
}

void Avatar::decide(){
    if(immunity_duration)
        immunity_duration--;

    /* Recompute Djikstra distances only if Agent position has changed. */
    if(_environment->getEDG() == EnvironmentGlobalDirection::NONE && moveRandomly()){
        if(immunity_duration)
            computeDjikstra(true);
        else
            computeDjikstra();
    }else{
        if(moveOnGlobalDirection()){
            if(immunity_duration)
                computeDjikstra(true);
            else
                computeDjikstra();
        }
    }
}

bool Avatar::moveOnGlobalDirection(){
    switch(_environment->getEDG()){
    case EnvironmentGlobalDirection::UPWARD:
        if(_y > 0){
            Agent* upward_agent = _environment->getAgent(_x, _y - 1);
            if(upward_agent && upward_agent->eatable()){
                defender_eaten++;
                if(upward_agent->winnable()){
                    won = true;
                }
                upward_agent->setAlive(false);
                immunity_duration = 10;
            }
            else if(!upward_agent){
                _environment->moveAgent(_x, _y, _x, _y - 1);
                return true;
            }
        }
        return false;
    case EnvironmentGlobalDirection::DOWNWARD:
        if(_y < _environment->getHeight() - 1){
            Agent* downward_agent = _environment->getAgent(_x, _y + 1);
            if(downward_agent && downward_agent->eatable()){
                defender_eaten++;
                if(downward_agent->winnable()){
                    won = true;
                }
                immunity_duration = 10;
                downward_agent->setAlive(false);
            }
            else if(!downward_agent){
                _environment->moveAgent(_x, _y, _x, _y + 1);
                return true;
            }
        }
        return false;
    case EnvironmentGlobalDirection::LEFTWARD:
        if(_x > 0){
            Agent* leftward_agent = _environment->getAgent(_x - 1, _y);
            if(leftward_agent && leftward_agent->eatable()){
                defender_eaten++;
                if(leftward_agent->winnable()){
                    won = true;
                }
                leftward_agent->setAlive(false);
                immunity_duration = 10;
            }else if(!leftward_agent){
                _environment->moveAgent(_x, _y, _x - 1, _y);
                return true;
            }
        }
        return false;
    case EnvironmentGlobalDirection::RIGHTWARD:
        if(_x < _environment->getWidth() - 1){
            Agent* rightward_agent = _environment->getAgent(_x + 1, _y);
            if(rightward_agent && rightward_agent->eatable()){
                defender_eaten++;
                if(rightward_agent->winnable()){
                    won = true;
                }
                rightward_agent->setAlive(false);
                immunity_duration = 10;
            }else if(!rightward_agent){
                _environment->moveAgent(_x, _y, _x + 1, _y);
                return true;
            }
        }
        return false;
    default:
        return false;
    }
    return false;
}

bool Avatar::moveRandomly(){
    std::vector<std::pair<uint16_t, uint16_t>> neighbours = _environment->getNeighbours(_x, _y);

    std::vector<std::pair<uint16_t, uint16_t>> reachable_squares;
    for(uint16_t i  = 0; i < neighbours.size(); i++){
        if(!_environment->getAgent(neighbours[i].first,neighbours[i].second)){
            reachable_squares.push_back(neighbours[i]);
        }
    }
    if(reachable_squares.size() > 0){
        std::shuffle(reachable_squares.begin(), reachable_squares.end(), *_environment->getRandomEngine());
        uint16_t random_x = reachable_squares.back().first;
        uint16_t random_y = reachable_squares.back().second;
        _environment->moveAgent(_x, _y, random_x, random_y);
        return true;
    }
    return false;
}

uint64_t Avatar::getEnergy() const{
    return defender_eaten;
}

bool Avatar::huntable() const{
    return true;
}

QColor Avatar::getColor() const {
    return (immunity_duration) ? QColor(245,161,66) : Qt::yellow;
}

std::string Avatar::to_string() const{
    return "A";
}

bool Avatar::winnable() const{
    return won;
}
