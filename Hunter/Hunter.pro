QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Agent.cpp \
    Avatar.cpp \
    Defender.cpp \
    Environment.cpp \
    Hunter.cpp \
    HunterOpenglWidget.cpp \
    Properties.cpp \
    PropertiesReader.cpp \
    Scheduler.cpp \
    Wall.cpp \
    Winner.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    Agent.hpp \
    Avatar.hpp \
    Defender.hpp \
    Environment.hpp \
    Hunter.hpp \
    HunterOpenglWidget.hpp \
    MainWindow.hpp \
    Properties.hpp \
    PropertiesReader.hpp \
    Scheduler.hpp \
    Wall.hpp \
    Winner.hpp

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
