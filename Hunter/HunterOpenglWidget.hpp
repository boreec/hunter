#ifndef HUNTEROPENGLWIDGET_HPP
#define HUNTEROPENGLWIDGET_HPP
#include <QOpenGLWidget>
#include <QPainter>
#include "Environment.hpp"
#include "Properties.hpp"
class HunterOpenGLWidget : public QOpenGLWidget
{
private:
    const Environment* environment;
    const Properties* properties;
public:
    HunterOpenGLWidget(QWidget* widget, const Environment* environment, const Properties* p);
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;
};

#endif // HUNTEROPENGLWIDGET_HPP
