#ifndef ENVIRONMENT_HPP
#define ENVIRONMENT_HPP
#include <memory>
#include <random>
#include <vector>
#include "Agent.hpp"
#include "Properties.hpp"

class Agent;

/*
 * The direction that most of the huntable agents will follow.
 * If set to NONE, the agent will move randomly.
 * */
enum class EnvironmentGlobalDirection{
    NONE,
    UPWARD,
    DOWNWARD,
    LEFTWARD,
    RIGHTWARD
};

class Environment{
private:
    uint16_t width;
    uint16_t height;
    std::vector<std::unique_ptr<Agent>> grid;
    std::default_random_engine* random_engine;
    const Properties *properties;
    EnvironmentGlobalDirection EDG;

    /*
     * Contains every neighbours for each coordinates.
     * This vector is computed and stored in memory beforehand.
     * */
    std::vector<std::vector<std::vector<std::pair<uint16_t, uint16_t>>>> neighboorhood;

    void computeVNNeighbours();
public:
    Environment(const Properties *p);
    void initialize_walls();

    void removeDeadAgents();

    void placeAgents(std::vector<std::unique_ptr<Agent>> &a);

    /*
     * Add an agent to the environment at the given coordinates.
     * If the coordinates are already occupied, the agent is not
     * added and false is returned. Otherwise agent is added and
     * true is returned.
     * */
    bool addAgent(std::unique_ptr<Agent>, uint16_t x, uint16_t y);
    void removeAgent(uint16_t x, uint16_t y);

    /*
     * Return a read-only pointer for a given cell.
     * If the cell is empty, nullptr is returned.
     * */
    Agent* getAgent(uint16_t x, uint16_t y) const;

    /*
     * Return the list of every existing agents in the environment.
     * */
    const std::vector<Agent*> getAgents();

    void moveAgent(const uint16_t agent_x, const uint16_t agent_y, const uint16_t target_x, const uint16_t target_y);
    std::string to_string() const;
    void setEDG(EnvironmentGlobalDirection edg);
    EnvironmentGlobalDirection getEDG() const;
    uint16_t getWidth() const;
    uint16_t getHeight() const;
    std::vector<std::pair<uint16_t, uint16_t>> getNeighbours(uint16_t x, uint16_t y) const;
    std::default_random_engine* getRandomEngine() const;

    /*
     * return for every huntable agent, its Djikstra distance for every cells.
     * */
    std::vector<std::vector<std::vector<int>>> getTargetsDjikstra() const;
};

#endif // ENVIRONMENT_HPP
