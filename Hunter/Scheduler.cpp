#include "Scheduler.hpp"

void Scheduler::SCHEDULE_SEQUENTIALLY(const std::vector<Agent*> agents, uint64_t& targets, bool& winnable, bool& won){
    targets = 0;
    winnable = false;
    for(unsigned int i = 0; i < agents.size(); i++){
        if(agents[i]->huntable()){
            if(agents[i]->getEnergy() >= 4){
                winnable = true;
            }
            if(agents[i]->winnable()){
                won = true;
            }
            targets += 1;
        }
        agents[i]->decide();
    }
}
void Scheduler::SCHEDULE_RANDOMLY(const std::vector<Agent*> agents, uint64_t& targets, bool& winnable, bool& won){
    targets = 0;
    winnable = false;
    won = false;
    std::vector<uint32_t> v;
    for(uint32_t i = 0; i < agents.size(); i++){
        v.push_back(i);
    }

    auto rng = std::default_random_engine {};
    std::shuffle(std::begin(v), std::end(v), rng);

    for(unsigned int i = 0; i < v.size(); i++){
        if(agents[v[i]]->huntable()){
            if(agents[v[i]]->getEnergy() >= 4)
                winnable = true;
            if(agents[v[i]]->winnable()){
                won = true;
            }
            targets += 1;
        }
        agents[v[i]]->decide();
    }
}
