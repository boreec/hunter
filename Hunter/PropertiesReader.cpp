#include "PropertiesReader.hpp"

Properties* PropertiesReader::getProperties(std::string filename){
  Properties* props = nullptr;
  std::ifstream file(filename);
  if(file.is_open()){
    std::string line;
    while(std::getline(file, line)){
      std::string key = "";
      std::string value = "";

      std::size_t _found = line.find("=");
      if(_found != std::string::npos){
    key = line.substr(0, _found);
    value = line.substr(_found + 1, std::string::npos);

    // remove newline char if necessary
    if(value[value.length() - 1] == '\n'){
      value.pop_back();
    }

    if(props == nullptr){
      props = new Properties();
    }
    registerProperties(props, key, value);
      }
    }
  }
  return props;
}

void PropertiesReader::registerProperties(Properties* p, std::string key, std::string value){
    if(p != nullptr){
        if(key == "HUNTER_QUANTITY"){
          p->setHunterQuantity(std::stoul(value));
        }
        else if(key == "AVATAR_QUANTITY"){
            p->setAvatarQuantity(std::stoul(value));
        }
        else if(key == "DEFENDER_LIFE"){
            p->setDefenderLife(std::stoul(value));
        }
        else if(key == "SCHEDULER"){
            p->setScheduler(value);
        }
        else if(key == "TORUS"){
            p->setTorus(std::stoul(value));
        }
        else if(key == "ENVIRONMENT_WIDTH"){
            p->setEnvironmentWidth(std::stoul(value));
        }
        else if(key == "ENVIRONMENT_HEIGHT"){
            p->setEnvironmentHeight(std::stoul(value));
        }
        else if(key == "WALLS_PERCENT"){
            p->setWallsPercent(std::stoul(value));
        }
        else if(key == "SEED"){
            p->setSeed(std::stoul(value));
        }
        else if(key == "VIEW_WIDTH"){
            p->setViewWidth(std::stoul(value));
        }
        else if(key == "VIEW_HEIGHT"){
            p->setViewHeight(std::stoul(value));
        }
        else if(key == "CELL_SIZE"){
            p->setCellSize(std::stoul(value));
        }
        else if(key == "GRID"){
            p->setGrid(std::stoul(value));
        }
        else if(key == "DELAY"){
            p->setDelay(std::stoul(value));
        }
        else if(key == "TICK_LIMIT"){
            p->setTickLimit(std::stoul(value));
        }
        else if(key == "CSV_EXPORT"){
            p->setCSVExport(std::stoul(value));
        }
    }
}
