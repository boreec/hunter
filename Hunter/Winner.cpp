#include "Winner.hpp"

Winner::Winner(uint16_t x, uint16_t y, Environment *e) : Agent(x, y, e){

}

void Winner::decide(){
    //do nothing
}

std::string Winner::to_string() const{
    return "*";
}

QColor Winner::getColor() const{
    return Qt::magenta;
}

bool Winner::huntable() const{
    return false;
}

bool Winner::eatable() const{
    return true;
}

bool Winner::winnable() const{
    return true;
}
