#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QMainWindow>
#include <QTimer>
#include "Avatar.hpp"
#include "Defender.hpp"
#include "Environment.hpp"
#include "Hunter.hpp"
#include "HunterOpenglWidget.hpp"
#include "Properties.hpp"
#include "Scheduler.hpp"
#include "Winner.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(const Properties* p, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void updateEnvironment();

private:
    Ui::MainWindow *ui;
    Environment* environment;
    HunterOpenGLWidget* opengl_widget;
    QTimer* timer;
    QLabel* tick_label;
    QLabel* game_result_label;
    QLabel* delay_label;

    const Properties* properties;
    uint16_t tick;
    int incremental_delay;

    // The environment contains agent that can win (they ate enough defenders).
    bool winnable;

    // The winners agents have been added to the environment, it's not necessary to add anymore.
    bool winners_added;

    // The game has been won by the agents (a Winner has been eaten).
    bool won;

    void initializeEnvironment();
    void initializeGUI();
    void updateGUI();
    void addDefenders();
    void addWinners();
    void restartAll();
    void keyReleaseEvent(QKeyEvent *k);
};
#endif // MAINWINDOW_HPP
