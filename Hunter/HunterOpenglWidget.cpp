#include "HunterOpenglWidget.hpp"
#include <iostream>

HunterOpenGLWidget::HunterOpenGLWidget(QWidget* widget, const Environment* e, const Properties *p): QOpenGLWidget(widget), environment(e), properties(p)
{

}

void HunterOpenGLWidget::initializeGL() {
    glClearColor(0, 0, 0, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void HunterOpenGLWidget::paintGL(){
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QPainter painter(this);
    std::string cell_text = "";

    for(uint16_t i = 0; i < environment->getHeight(); i++){
        for(uint16_t j = 0; j < environment->getWidth(); j++){
            QColor brush_color = Qt::black;
            cell_text = "";
            // If an agent exists for a given coordinate, takes its color.
            if(auto agent = environment->getAgent(j, i)){
                brush_color = agent->getColor();
                if(agent->huntable())
                    cell_text = std::to_string(agent->getEnergy());
            }
            painter.setBrush(QBrush(brush_color, Qt::SolidPattern));
            painter.drawRect(j * properties->getCellSize(), i * properties->getCellSize(), properties->getCellSize(), properties->getCellSize());
            painter.setPen(Qt::black);
            painter.drawText(j * properties->getCellSize() + properties->getCellSize()/2, i * properties->getCellSize() + properties->getCellSize()/2, QString::fromStdString(cell_text));
        }
    }
}


void HunterOpenGLWidget::resizeGL(int w, int h){
    glViewport(0, 0, w, h);
}

