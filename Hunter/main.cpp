#include "MainWindow.hpp"
#include "Properties.hpp"
#include "PropertiesReader.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
    Properties* properties;

    properties = new Properties();
    if(argc > 1){
      delete properties;
      properties = PropertiesReader::getProperties(argv[1]);
    }
    QApplication a(argc, argv);
    MainWindow w(properties);
    w.show();
    return a.exec();
}
