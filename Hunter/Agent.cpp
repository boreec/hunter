#include "Agent.hpp"

Agent::Agent(uint16_t x, uint16_t y, Environment* e) : _x(x), _y(y), _environment(e)
{
    computeDjikstra();
    alive = true;

}

void Agent::setX(uint16_t posX){
    _x = posX;
}

void Agent::setY(uint16_t posY){
    _y = posY;
}

void Agent::setAlive(bool b){
    alive = b;
}

bool Agent::isAlive() const{
    return alive;
}

uint64_t Agent::getEnergy() const{
    return 0;
}

bool Agent::eatable() const{
    return false;
}

bool Agent::winnable() const{
    return false;
}

void Agent::computeDjikstra(bool reverse){

    // Initialize grid with -1 values. -1 means that a cell has not been
    // computed yet.
    djikstra_grid = std::vector<std::vector<int>>();
    for(int i = 0; i < _environment->getHeight(); i++){
        std::vector<int> row;
        for(int j = 0; j < _environment->getWidth(); j++){
            row.push_back(-1);
        }
        djikstra_grid.push_back(row);
    }

    int d_value = 0;
    if(reverse)
        d_value = _environment->getHeight() + _environment->getWidth();
    djikstra_grid[_y][_x] = d_value;

    std::vector<std::pair<uint16_t, uint16_t>> neighbours = _environment->getNeighbours(_x, _y);
    std::list<std::pair<uint16_t, uint16_t>> neighbours_to_vist(neighbours.begin(), neighbours.end());

    while(neighbours_to_vist.size() > 0){
        if(reverse)
            d_value--;
        else
            d_value++;
        std::list<std::pair<uint16_t, uint16_t>> tmp;
        for(std::list<std::pair<uint16_t, uint16_t>>::iterator it = neighbours_to_vist.begin(); it != neighbours_to_vist.end(); it++){
            if(djikstra_grid[it->second][it->first] == -1){
                djikstra_grid[it->second][it->first] = d_value;
                std::vector<std::pair<uint16_t, uint16_t>> current_neighbours = _environment->getNeighbours(it->first, it->second);
                for(uint16_t n = 0; n < current_neighbours.size(); n++){
                    if(djikstra_grid[current_neighbours[n].second][current_neighbours[n].first] == -1){
                        tmp.push_back(current_neighbours[n]);
                    }
                }
            }
        }
        neighbours_to_vist = std::list<std::pair<uint16_t, uint16_t>>(tmp);
    }
}

std::vector<std::vector<int>> Agent::getDjikstraGrid() const{
    return djikstra_grid;
}
