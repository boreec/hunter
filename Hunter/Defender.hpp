#ifndef DEFENDER_HPP
#define DEFENDER_HPP
#include "Agent.hpp"

class Defender : public Agent
{
private:
    uint16_t _life;
public:
    Defender(uint16_t x, uint16_t y, Environment *e, uint16_t life);

    virtual void decide() override;
    virtual std::string to_string() const override;
    virtual QColor getColor() const override;
    virtual bool huntable() const override;
    virtual bool eatable() const override;
};

#endif // DEFENDER_HPP
