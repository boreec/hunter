#include <fstream>
#include <string>
#include "Properties.hpp"

#ifndef __PROPERTIES_READER_HPP__
#define __PROPERTIES_READER_HPP__
class PropertiesReader{
private:
  static void registerProperties(Properties*, std::string, std::string);
public:
  static Properties* getProperties(std::string);
};
#endif
