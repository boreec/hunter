#ifndef HUNTER_HPP
#define HUNTER_HPP
#include "Agent.hpp"

class Hunter : public Agent
{
public:
    Hunter(uint16_t x, uint16_t y, Environment *e);

    virtual void decide() override;
    virtual std::string to_string() const override;
    virtual QColor getColor() const override;
    virtual bool huntable() const override;
};

#endif // HUNTER_HPP
