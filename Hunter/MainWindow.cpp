#include "MainWindow.hpp"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(const  Properties* p, QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      properties(p)
{

    tick = 0;
    winnable = false;
    winners_added = false;
    won = false;
    incremental_delay = properties->getDelay();
    ui->setupUi(this);
    initializeEnvironment();
    initializeGUI();


    updateEnvironment();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateEnvironment()));
    timer->start(incremental_delay);

}

void MainWindow::initializeGUI(){
    //initialize widgets
    opengl_widget = new HunterOpenGLWidget(nullptr, environment, properties);
    tick_label = new QLabel("0");
    delay_label = new QLabel(QString("delay : %1").arg(properties->getDelay()));
    game_result_label = new QLabel("Game result : undefined");
    opengl_widget->setFixedSize(properties->getCellSize() * environment->getWidth(),properties->getCellSize() * environment->getHeight());
    ui->scrollAreaWidgetContents->layout()->addWidget(opengl_widget);
    ui->gridLayout->addWidget(game_result_label,1,0);
    ui->gridLayout->addWidget(tick_label,1,1);
    ui->gridLayout->addWidget(delay_label,1,2);

    //initialize window
    setFixedSize(properties->getViewWidth(), properties->getViewHeight());
    setWindowTitle(QString("Hunter SMA - Cyprien Borée"));
}

void MainWindow::initializeEnvironment(){

    environment = new Environment(properties);
    std::vector<std::unique_ptr<Agent>> agents;
    int hunters = properties->getHunterQuantity();
    int targets = properties->getAvatarQuantity();
    for(int i = 0; i < hunters; i++){
        agents.push_back(std::make_unique<Hunter>(0,0,environment));
    }
    for(int i = 0; i < targets; i++){
        agents.push_back(std::make_unique<Avatar>(0,0,environment));
    }
    environment->placeAgents(agents);
}

void MainWindow::addDefenders(){
    uint16_t defenders_amount = environment->getWidth() * environment->getHeight() * 0.01;
    uint16_t defenders_life = properties->getDefenderLife();
    std::vector<std::unique_ptr<Agent>> defenders;
    for(uint16_t i = 0; i < defenders_amount; i++){
        defenders.push_back(std::make_unique<Defender>(0,0,environment, defenders_life));
    }
    environment->placeAgents(defenders);
}

void MainWindow::addWinners(){
    uint16_t winners_amount = environment->getWidth() * environment->getHeight() * 0.01;
    std::vector<std::unique_ptr<Agent>> winners;
    for(uint16_t i = 0; i < winners_amount; i++){
        winners.push_back(std::make_unique<Winner>(0,0,environment));
    }
    environment->placeAgents(winners);
}


void MainWindow::updateEnvironment(){
    environment->removeDeadAgents();

    if(tick % 15 == 0){
        addDefenders();
    }
    uint64_t targets = 0;

    const std::vector<Agent*> agents = environment->getAgents();

    if(properties->getScheduler() == "RANDOM"){
        Scheduler::SCHEDULE_RANDOMLY(agents, targets, winnable, won);
    }else{
        Scheduler::SCHEDULE_SEQUENTIALLY(agents, targets, winnable, won);
    }
    if(winnable && !winners_added){
        addWinners();
        winners_added = true;
    }
    updateGUI();
    tick++;
    if(!targets){
        game_result_label->setText("Game results : Hunter(s) won.");
        timer->stop();
    }
    if(won){
        game_result_label->setText("Game results : Avatar(s) won.");
        timer->stop();
    }
}

void MainWindow::updateGUI(){
    tick_label->setText(QString("tick %1").arg(tick));
    opengl_widget->update();
}

void MainWindow::keyReleaseEvent(QKeyEvent *k){
    if(k->key() == Qt::Key_Left)
    {
        environment->setEDG(EnvironmentGlobalDirection::LEFTWARD);
    }
    else if(k->key() == Qt::Key_Right){
        environment->setEDG(EnvironmentGlobalDirection::RIGHTWARD);
    }
    else if(k->key() == Qt::Key_Up){
        environment->setEDG(EnvironmentGlobalDirection::UPWARD);
    }
    else if(k->key() == Qt::Key_Down){
        environment->setEDG(EnvironmentGlobalDirection::DOWNWARD);
    }
    else if(k->key() == Qt::Key_Space){
        restartAll();
    }
    else if(k->key() == Qt::Key_X){
        incremental_delay = incremental_delay * 0.5;
        timer->stop();
        timer->start(incremental_delay);
        delay_label->setText(QString("delay : %1").arg(incremental_delay));
    }
    else if(k->key() == Qt::Key_W){
        incremental_delay = incremental_delay * 2;
        timer->stop();
        timer->start(incremental_delay);
        delay_label->setText(QString("delay : %1").arg(incremental_delay));
    }
}

void MainWindow::restartAll(){
    tick = 0;
    won = false;
    winners_added = false;
    winnable = false;
    incremental_delay = properties->getDelay();
    timer->stop();
    ui->setupUi(this);
    initializeEnvironment();
    initializeGUI();
    updateEnvironment();
    timer->start(incremental_delay);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete environment;
    delete timer;
    delete tick_label;
    delete properties;
}

