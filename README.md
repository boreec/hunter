# Hunter

Author : [Cyprien Borée](https://boreec.fr)

![](http://boreec.fr/wp-content/uploads/2020/11/Hunter.gif)

This is a Multi-agent system in a 2D discrete environment with cognitive agents :
- **Avatar** (in yellow): An agent possibly controled by the user with keyboard (random movements otherwise).
- **Hunter** (in green): An agent using the Djikstra's algorithm to move toward **Avatar** agents. If a **Hunter** reaches an **Avatar**, it's game over.
- **Defender** (in red): These agents appear every once in a while. When enough of them have been eaten by an **Avatar** it spawns a **Winner**. When an **Avatar** eats one, it becomes *invincible* and makes **Hunter** run away using an inversed Djikstra.
- **Winner** (in pink): Appear once an **Avatar** ate enough **Defender** agents.
- **Wall** (in brown): Passive agents acting as walls.

## Compilation

The project is written in C++17, so make sure to use a supported compiler. Also, it uses the library Qt, so you need at least **qmake** to build the Makefile :

```
$ cd Hunter
$ qmake Hunter.pro
$ make
$ make distclean # to eventually clean files afterward
```

Another way to do is to install the IDE QtCreator and open the file Hunter.pro with it.

## Execution

The simulation can be built on a set of parameters given in a `.property` file. The following properties are :
- **Agents properties :**
    - **HUNTERS_QUANTITY** : The amount of Hunters in the environment.
    - **AVATAR_QUANTITY** : The amount of Avatars in the environment.
    - **DEFENDER_LIFE** : The life of a defender in tick.
- **Environment properties :**
    - **ENVIRONMENT_WIDTH** : Number of cells in width.
    - **ENVIRONMENT_HEIGHT** : Number of cells in height.
    - **WALLS_PERCENT** : Density of walls randomly placed in environment.
    - **SEED** : Seed used to generate randomness. If set to `0`, entropy is used.
- **View Properties :**
    - **VIEW_WIDTH** : Window's width in pixels.
    - **VIEW_HEIGHT** : Window's height in pixels.
    - **CELL_SIZE** : Cell's size in pixels.
    - **GRID** : Boolean to display or not grid between cells.
- **Scheduling properties :**
    - **SCHEDULER** : Random scheduling if set to `RANDOM`, sequential otherwise.
    - **DELAY** : Time between every update in milliseconds.
    - **TICK_LIMIT** : Number of turns before ending simulation. If set to `0` simulation is infinite.

The format of the file to write down these properties is :

```txt
PARAMETER=VALUE
PARAMETER=VALUE
```
You can find an example in `medium_experiment.property` and `small_experiment.property` :

```
$ cd Hunter/
$ qmake Hunter.pro
$ make
$ ./Hunter ../medium_experiment.property 
```

Moreover, during execution the following keys can be used :
- `spacebar` : Restart simulation.
- `W` : Slow down simulation.
- `X` : Speed up simulation.
- `←` : Set all **Avatar** agents direction to the left.
- `↑` : Set all **Avatar** agents direction to the top.
- `→` : Set all **Avatar** agents direction to the right.
- `↓` : Set all **Avatar** agents direction to the bottom.
